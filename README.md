# Tests unitaires en Java

Apprenez les tests unitaires en Java grâce à ce projet. De plus, nos tutoriels Markdown vous permettrons de suivre un cours débutant en la matière.

## Utilisation du projet
Chaque leçon est disponible dans l'un des dossiers du projet. Chaque dossier sont dénommés par 0 - {Nom de la leçon}.

Dans le dossier, vous retrouverez les fichiers .java et un tutoriel Markdown qui explique le fonctionnement de l'implantation. De plus, les fichiers .java sont documentés et commenter pour facilité l'apprentissage.