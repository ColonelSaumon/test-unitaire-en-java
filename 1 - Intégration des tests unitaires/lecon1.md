# Leçon 1 : Intégration des tests unitaires


## Qu'est-ce qu'un test ?


Dans le monde de la programmation, il est fréquent d'avoir le besoin de tester notre code. Ils nous permettent de vérifier la bonne marche de notre code. Lorsque nous commençons en programmation, nous testons généralement nos fonctions directement dans le main. Or, dès que nous souhaitons utiliser l'encapsulation, il est nécessaire de briser ses principes pour tester nos codes.


Le même problème se produit lorsque notre code fait des milliers de lignes ou qu'une fonction doit appeler à une cascade de celles-ci. Nos tests faits à « main » deviennent complexes. Une bonne pratique à acquérir est les tests automatisés. Il en existe plusieurs les tests d'intégration, les tests d'acceptation, les tests unitaires et plusieurs autres.


Dans ce cours, il sera question des tests unitaires. Ces derniers servent à vérifier le fonctionnement d'une *unité* du code. En général, une unité réfère à une fonction. En Java, nos tests unitaires testeront les méthodes de vos classes.


De plus, les tests unitaires ont d'autres avantages. Les tests sont **reproductibles**. Cela signifie que d'une version à l'autre, vos tests seront effectués de nouveau et qu'ils ne changeront pas tant que vous ne modifiez pas le code du test. Aussi, les tests unitaires sont plus **compréhensibles**. Grâce à sa syntaxe, nous pouvons comprendre facilement le but du test et ses origines. Enfin, comparativement aux tests personnalisés réalisés à la main, les tests unitaires sont **documentés**.


Dernier avantage, les tests unitaires sont, généralement, faciles à écrire. Il existera un test par méthode. Ainsi, nous n'avons pas besoin de comprendre le fonctionnement de la classe au grand complet. Nous avons besoin uniquement de comprendre le fonctionnement de notre propre méthode. Chaque classe de notre projet aura **sa propre classe de test**.


## Comment guider nos tests ?


La première question que vous devriez vous poser est le nombre de tests nécessaire. Prenez par exemple une méthode qui détermine si une année est bissextile. En théorie, il faudrait tester tous les entiers possibles (*soit une infinité*). Or, nous savons qu'il est impossible de tester toutes ses options. Nous devrons donc sélectionner quelques années **charnières** et les tester.


Aussi, une fois tester, devons-nous recommencer le test à plusieurs reprises ? Par chance, la réponse est **non**. En effet, à moins d'avoir des erreurs d'implantation (*par exemple une fuite de mémoire*), une méthode retourne toujours la même valeur. Ainsi, nous pouvons dire qu'une fois tester, la méthode n'a pas besoin d'être retestée.


Dès lors, pour répondre à la question : Comment guider nos tests ? La réponse consiste en deux préceptes :


+ Lorsque la méthode fonctionne pour **quelques valeurs**, elle fonctionne pour **toutes les valeurs**.


+ Lorsque la méthode fonctionne **une fois**, elle fonctionne **toutes les fois**.


## Structure d'un test


Un test aura toujours la même structure, nous aurons besoin d'une donnée d'entrée, d'un oracle et d'une donnée de sortie. Le premier élément est la donnée d'entrée. Celle-ci est la valeur que vous transmettrez à votre fonction. Votre test vérifiera le **résultat** de votre méthode avec votre donnée d'entrée. En d'autres mots, est-ce que la méthode renvoie la valeur *y* quand on lui donne la valeur *x* ?


Le second élément est l'oracle. Ce dernier est un outil qui prédit le résultat du test en fonction de la donnée d'entrée. Elle est la bonne réponse à votre test. Reprenez l'exemple de l'année bissextile. Si nous testons l'année 2020, nous savons qu'elle est bissextile. Donc, l'oracle de ce test sera **vrai**, car vrai représente la prédiction du résultat de notre test.


Enfin, le troisième élément est la donnée de sortie. Ceci correspond simplement au résultat de votre méthode. Il s'agit donc de la valeur qui sera vérifiée avec l'oracle. Dans notre exemple, si la méthode retourne **fausse**, le test signalera un **échec**, car l'oracle est vrai. Dans le cas contraire, le test signalera un **succès**.


La figure suivante montre le comportement d'un test unitaire. Le test vérifie que la donnée retournée par la fonction est la même que celle prévue par l'oracle.


![Schéma d'un test unitaire. Le test vérifie que la donnée retournée par la fonction est la même que celle prévue par l'oracle.](figures/tests_schema.png)


## Framework de test unitaire en Java : JUnit


Pour réaliser nos tests, nous utiliserons le framework JUnit. Ce framework fait partie d'un ensemble de frameworks dédié aux tests unitaires. Il existe une version de ce dernier pour plusieurs langages : [CUnit](http://cunit.sourceforge.net/) pour le langage C, [XUnit.net](https://xunit.net/docs/getting-started/netfx/visual-studio) pour .NET Framework de Mircosoft, [PyUnit](https://wiki.python.org/moin/PyUnit) pour le langage Python, [PHPUnit](https://phpunit.de/) pour le langage PHP et plusieurs autres.


En Java, JUnit ne fait pas partie de la librairie standard (JDK). Il est donc nécessaire d'ajouter ce dernier à nos projets. Heureusement, si vous utilisez un IDE, la plupart d'entre eux intègrent directement le framework. Dans ce cours, nous implanterons le framework pour l'IDE Intellij. De plus, dans ce cours, nous travaillerons avec la cinquième version de la librairie.


## Intégration de JUnit à notre projet Intellij


Pour commencer, il est important d'ajouter la bibliothèque (librairie) JUnit dans le projet. Il est possible que cette dernière soit déjà présente. Néanmoins, par souci de compréhension, nous présenterons l'ajout depuis le début. Pour commencer, il faut **ajouter manuellement** la bibliothèque. Pour cela, il suffit d'accéder à la structure de notre projet. Vous pouvez y accéder par le menu **File | Project Structure** ou par le raccourcis `Ctrl + Shift + Alt + S`. Vous pouvez aussi y accéder par l'icône encerclée sur l'image suivante.


![Icône d'accès à la structure de projet dans Intellij](figures/open_project_structures.png)


Par la suite, sélectionnez le sous-menu **Libraries** (encerclé en rouge sur la figure suivante). Par la suite, sélectionnez l'option **From Maven** (encerclé en vert sur la figure suivante) dans le menu contextuel **+** (encerclé en bleu sur la figure suivante).


![Ouverture de la fenêtre d'ajout de la bibliothèque.](figures/add_libraries.png)


Dans la fenêtre qui apparaît, recherche le package `org.junit.jupiter:junit-jupiter` et sélectionnez la version que vous souhaitez. Une fois que vous aurez appliqué les changements, Intellij se chargera d'ajouter la bibliothèque à votre projet.


## Créer un test


Créez, soit à la racine de votre projet ou dans un package dédié, une nouvelle classe. En général, le nom de votre classe de test sera similaire à celle de la classe testée en plus d'ajouter le mot **Test**. Pour cet exemple, nous utiliserons la classe ``AlgoDates`` ([disponible ici](AlgoDates.java)) qui contient des algorithmes liés aux dates. Pour les tests, nous utiliserons la classe `AlgoDatesTests` ([disponible ici](AlgoDatesTests.java)).


Une fois votre classe de test créée, vous devez ajouter importer dans votre fichier la fonction `assertTrue` et la classe `Test`. Pour cela, vous avez besoin des deux lignes suivantes :


```java
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
```


Par la suite, vous devez créer une fonction pour tester la fonction désirée. Personnellement, j'aime utiliser le même nom de fonction et ajouter le mot **Test**. Dans le code actuel, je vais tester la fonction `estBissextille`. Je vais alors appeler ma fonction de test `estBissextilleTest`. Cette fonction ne prend aucun paramètre. Elle sera simplement appelée par l'exécuteur. Toutefois, pour que la fonction soit considérée comme un test, il faut y ajouter l'annotation `@Test`. Nous verrons dans les prochaines leçons l'utilité de cette annotation.


```java
@Test
public void estBissextilleTest() {
// Permet de vérifier que la réponse de la
// fonction est bien true.
assertTrue(AlgoDates.estBissextille(2020));
}
```


## Dans la prochaine leçon
Dans la prochaine leçon, nous verrons plus en détail chaque annotation possible de JUnit et les différentes assertions possibles (`assertTrue`).