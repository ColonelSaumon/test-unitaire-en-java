/**
 * Contient des algorithmes relatives aux dates.
 * @author ColonelSaumon
 * @licence MIT
 * @version 1.0
 * @since 1.0
 */

public class AlgoDates {
    /**
     * Détermine si l'année est bissextille
     * @param annee Année à analyser
     * @return True si l'année est bissextille, false sinon
     */
    public static boolean estBissextille(int annee) {
        return (annee % 4 == 0) && ((annee % 100 > 0) || (annee % 400 == 0));
    }
}
