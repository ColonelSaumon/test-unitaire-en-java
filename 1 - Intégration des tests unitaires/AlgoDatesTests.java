/**
 * Contient les tests unitaires des algorithmes relatives aux dates.
 * @author ColonelSaumon
 * @licence MIT
 * @version 1.0
 * @since 1.0
 */

// Il est nécessaire d'importer les classes suivantes.
// Il s'agit des éléments inspensable pour réaliser les tests.
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

public class AlgoDatesTests {
    /**
     * Permet d'exécuter une série de test sur la
     * fonction estBissextille de AlgoDates
     */
    @Test
    public void estBissextilleTest() {
        // Permet de vérifier que la réponse de la
        // fonction est bien true.
        assertTrue(AlgoDates.estBissextille(2020));
    }
}
